package com.lhy.sharding.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("t_order_1")
public class OrderEntity {

    @TableId(type = IdType.INPUT)
    private Long orderId;

    private BigDecimal price;

    private Long userId;

    private String status;
}
