package com.lhy.sharding.controller;

import com.lhy.sharding.dao.OrderDao;
import com.lhy.sharding.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private OrderDao orderDao;

    @GetMapping("/index")
    public String index(){

        return "index";
    }

}
