package com.lhy.sharding;

import com.lhy.sharding.dao.OrderDao;
import com.lhy.sharding.entity.OrderEntity;
import com.lhy.sharding.service.impl.OrderServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
class ShardingJdbcApplicationTests {

    @Autowired
    private OrderDao orderMapper;

    @Autowired
    private OrderServiceImpl orderService;

    @Test
    void contextLoads() {
    }

    @Test
    void testInsertOrder() {
        for (int i = 0; i < 20; i++) {
            orderMapper.insertOrder(new BigDecimal(i),Long.parseLong(String.valueOf(i)),"已发货");
        }
    }

    @Test
    void testSelectOrder() {
        List<Long> ids = new ArrayList<>();
        ids.add(973256069591597056L);
        ids.add(973259861095612417L);
        List<Map> maps = orderMapper.selectOrderByIds(ids);
        System.out.println(maps);
    }

    /**
     * 测试插入不同的库
     */
    @Test
    void testInsertDBOrder() {
        for (int i = 0; i < 10; i++) {
            orderMapper.insertOrder(new BigDecimal(i),1L,"已发货");
        }

        for (int i = 0; i < 10; i++) {
            orderMapper.insertOrder(new BigDecimal(i),2L,"已发货");
        }
    }



    /**
     * 未能分表
     */
    @Test
    void testInsertOrderByMybatis() {
        for (int i = 0; i < 20; i++) {
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setOrderId(Long.parseLong(String.valueOf(i)));
            orderEntity.setPrice(new BigDecimal(i));
            orderEntity.setUserId(Long.parseLong(String.valueOf(i)));
            orderEntity.setStatus("未发货");
            orderService.save(orderEntity);
        }
    }

}
